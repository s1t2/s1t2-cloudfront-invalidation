# Not a plug and play plugin though as it requires some configuration. You need to have these ready

    AWS_S3_REGION=
	AWS_CLOUDFRONT_DISTRO= (The cloudfront distribution ID)

    (Optional if using IAM role)
    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=	
