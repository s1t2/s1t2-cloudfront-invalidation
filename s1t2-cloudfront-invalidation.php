<?php
/*
  Plugin Name: S1T2 Cloudfront Invalidation
  Version: 1.0.2
  Author: S1T2
  Author URI: s1t2.com.au
  Description: Hooks into various save post actions to invalidate cloudfront caches.
*/

if ( ! defined( 'ABSPATH' ) ) exit;

use Aws\Cloudfront\CloudFrontClient;
use Aws\Exception\AwsException;

add_action('plugins_loaded', [CloudfrontInvalidation::get_instance(), 'init']);

// A little singleton
class CloudfrontInvalidation
{
	protected function __construct() { }
	private $aws_response;

	public static function get_instance()
	{
		static $instance = null;
		if ($instance === null) {
			$instance = new static();
		}
		return $instance;
	}

	public function init()
	{
		$actions = ['save_post', 'wp_trash_post'];
		foreach($actions as $item) {
			add_action( $item, [$this, 'createTheInvalidation'], 100, 1);
		};
		add_action('cache_enabler_site_cache_cleared', [$this, 'createInvalidation'], 100, 0);
	}

	public function createInvalidation()
	{
		$distributionId = $this->retrieveEnvVar('AWS_CLOUDFRONT_DISTRO');
		$region = $this->retrieveEnvVar('AWS_S3_REGION');
		$date = date('Y-m-d_h:i:s', time());
		$callerReference = $this->retrieveEnvVar('WP_HOME') . '_' . $date;

		if ( $this->checkIfLocalhost($callerReference) ) return;

		$paths = ['/*'];
		$quantity = 1;

		$cloudFrontClient = new Aws\CloudFront\CloudFrontClient([
			'version' => '2018-06-18',
			'region' => $region
		]);

		try {
			$result = $cloudFrontClient->createInvalidation([
				'DistributionId' => $distributionId,
				'InvalidationBatch' => [
					'CallerReference' => $callerReference,
					'Paths' => [
						'Items' => $paths,
						'Quantity' => $quantity,
					],
				]
			]);

			$message = '';

			if (isset($result['Location']))
			{
				$message = 'The invalidation location is: ' .
					$result['Location'];
			}

			$message .= ' and the effective URI is ' .
				$result['@metadata']['effectiveUri'] . '.';

			return $message;
		} catch (AwsException $e) {
			$error = 'Error: ' . $e->getAwsErrorMessage();
			error_log($error);
			return $error;
		}
	}

	public function createTheInvalidation($post_id)
	{
		if ( ! is_user_logged_in() ) return;
		$post = get_post($post_id);
		if ( $post->post_status !== 'publish' ) return;

		$distributionId = $this->retrieveEnvVar('AWS_CLOUDFRONT_DISTRO');
		$region = $this->retrieveEnvVar('AWS_S3_REGION');
		$date = date('Y-m-d_h:i:s', time());
		$callerReference = $this->retrieveEnvVar('WP_HOME') . '_' . $date;

		if ( $this->checkIfLocalhost($callerReference) ) return;

		$paths = ['/*'];
		$quantity = 1;

		$cloudFrontClient = new Aws\CloudFront\CloudFrontClient([
			'version' => '2018-06-18',
			'region' => $region
		]);

		$result = $this->createInvalidation();
		if ( strpos($result, 'Error') ) error_log($result);
	}

	public function retrieveEnvVar($envVar)
	{
		$id = function () use ($envVar) { return getenv($envVar); };
		return $id($envVar) ? $id($envVar) : '';
	}

	public function checkIfLocalhost($caller)
	{
		$local = preg_match( '/(.*)localhost(.*)/', $caller);
		return $local >= 1 ? true : false;
	}
}
